Files for Introduction to Genomics course Project: "Human Protein Atlas Image Classification"

Authors: Menoua Keshishian (mk4011@columbia.edu), Vinay Raghavan (vsr2119@columbia.edu)

Contents:

- **src/FastAI.ipynb**: Jupyter notebook for training and testing the single branch DNN
- **src/FastAI-2br.ipynb**: Jupyter notebook for training and testing the double branch DNN
- **src/FastAI-3br.ipynb**: Jupyter notebook for training and testing the triple branch DNN
- **src/FastAI-4br.ipynb**: Jupyter notebook for training and testing the quadruple branch DNN (inadequate performance, not included in report)
